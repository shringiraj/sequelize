var db = require('../models')
const User = db.user
const index = async (req, res) => {
    let limit = req.query.limit ? parseInt(req.query.limit) : 5
    let page = req.query.page ? parseInt(req.query.page) : 1
    let userModels;
    try {
         userModels = await User.findAndCountAll({
            limit:limit,
            offset:limit * (page - 1),
         });
    } catch (error) {
        console.log(error);
    }
    
    response = {
        data:userModels,
        params:req.query
    }
    res.status(200).json(response)
}
const view = async (req, res) => {
    const userId = req.params.id
    let userModel;
    try {
         userModel = await User.findByPk(userId);
    } catch (error) {
        console.log(error);
    }
    
    response = {
        data:userModel,
    }
    res.status(200).json(response)
}
const deleteUser = async (req, res) => {
    const userId = req.params.id
    
    let userModel;
    try {
         userModel = await User.destroy({where: {id:userId}});
    } catch (error) {
        console.log(error);
    }
    
    response = {
        data:userModel,
    }
    res.status(200).json(response)
}
const create = async (req, res) => {
    var post_body = req.body;
    // Return the POST message
    res.send(post_body);
    // try {
    //     var data = await User.build({firstName:req.body.firstName,lastName:req.body.lastName});    
    //     await data.save();
    //     //console.log(data)
    // } catch (error) {
    //     console.log(error);
    // }
    
    response = {
        data:'create Data',
        data1:req.body.firstName
    }
    res.status(200).json(response)
}

module.exports = {
    index,
    view,
    deleteUser,
    create
}