const router = require("express").Router();
const UserController = require("../controllers/UserController");

//get
//post
//put
//patch
//delete

router.get("/", UserController.index);
router.get("/:id", UserController.view);
router.post("/", UserController.create);
// router.put("/:id", UserController.update);
router.delete("/:id", UserController.deleteUser);


module.exports = router;