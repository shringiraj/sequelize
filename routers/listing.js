const router = require("express").Router();
const Listing = require("../models/Listing");
const City = require("../models/City");
//get
//post
//put
//patch
//delete

router.get("/", async (req, res) => {
    
    try {
        const model = await Listing.find();
        res.json(model);
    } catch (error) {
        res.send(error);
    }
});
router.get("/add", async (req, res) => {
    //res.send("API working.");
    const listing = await new Listing({
        name: "Manish",
        age: "String",
        height: "String",
        details: "String",
        address:"Shankar Nagar"
    });
    try {
        const listingSave = listing.save();
        res.send(listingSave);
    } catch (error) {
        res.send(error);
    }
});

router.get("/city", async (req, res) => {
    try {
        const model = await City.find();
        res.json(model);
    } catch (error) {
        res.send(error);
    }
});

router.get("/:id", async (req, res) => {
    //res.send("API working.");
    
    try {
        const model = await Listing.findById(req.params.id);
        res.json(model);
    } catch (error) {
        res.send(error);
    }
});


router.get("/city/add", async (req, res) => {
    //res.send("API working.");
    const city = await new City({
        cityName: "Raipur"
    });
    try {
        const citySave = city.save();
        res.send(citySave);
    } catch (error) {
        res.send(error);
    }
});



module.exports = router;