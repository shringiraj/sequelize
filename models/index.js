const { Sequelize,DataTypes } = require('sequelize');
require('dotenv').config()

let db = {};
db.Sequelize = Sequelize;
db.sequelize = new Sequelize('sequelize', process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  dialect: process.env.DB_DIALECT,/* one of 'mysql' | 'mariadb' | 'postgres' | 'mssql' */
  pool:{max:5,min:0,idle:30000}
});
db.sequelize.authenticate()
.then(()=>{
    console.log("connected");
})
.catch(err =>{
    console.log(`Error : ${err}`)
})

db.user = require('./user')(db.sequelize,DataTypes);

db.sequelize.sync({
    force:false, 
    // match:/-test$/
})
.then(()=>{
    console.log("yes re sync")
})
module.exports = db;