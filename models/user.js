module.exports =(sequelize,DataTypes)=>{ 
    const User = sequelize.define('User', {
    // Model attributes are defined here
    firstName: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING
      // allowNull defaults to true
    }
  }, {
    //timestamps: false,
    tableName: 'user',
    createdAt:false,//'created_at',
    updatedAt:false,
    engine:'InnoDB',//InnoDB or MYISAM
  });
  return User;
}