const express = require('express')
const db = require('./models/index');
const UserController = require('./controllers/UserController');
const app = express()
const cors = require("cors");
require('dotenv').config()
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.get('/',  (req, res) => {
    console.log(['Project','is','working','now','.']);
    return res.status(200).json(['Project','is','working','now','.']);
//   const User = db.user
//   await User.create({firstName:'Shringirak',lastName:'Dewangan'})
})
app.use(cors());//for Cross-origin resource sharing

app.use("/api/user",require('./routers/userRouter'));

app.listen(process.env.PORT,()=>{ 
    console.log(`Up at http://localhost:${process.env.PORT}`)
}) 